package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bytemanipulation.Version;

public class Frame extends JFrame implements ActionListener {

	private EasyChat easyChat;

	private JPanel panel = new JPanel();
	private JComboBox comboBox = new JComboBox();
	private JButton button = new JButton("Installieren");

	public Frame(EasyChat easyChat) {
		this.easyChat = easyChat;
		initGui();
	}

	private void initGui() {

		for (String opt : Version.getVersionList()) {
			// Exclude new Labymod3 fore now
			if (!opt.equals(Version.V1_8_9_LABY_MOD3.version)) {
				comboBox.addItem(opt);
			}
		}
		comboBox.setSelectedIndex(0);

		button.addActionListener(this);

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.add(comboBox);
		panel.add(button);
		panel.setBorder(BorderFactory.createLineBorder(panel.getBackground(), 5));

		comboBox.setBorder(new EmptyBorder(5, 5, 5, 0));

		this.getContentPane().add(panel);

		try {
			BufferedImage img = ImageIO.read(
					new ByteArrayInputStream(Base64.getDecoder().decode(EasyChat.LOGO_EASYCHAT_BASE_64.getBytes())));
			this.setIconImage(img);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setTitle("EasyChat [_Skewy]");
		this.pack();
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		easyChat.onButtonClick("" + this.comboBox.getSelectedItem());
	}

}
