package bytemanipulation;

public enum Version {

	V1_8_8("1.8.8"), V1_8_8_LABY_MOD2("LabyMod-1.8.8"), V1_8_9("1.8.9"), V1_8_9_LABY_MOD3("LabyMod-3-1.8.9");

	public String version = "";

	Version(String version) {
		this.version = version;
	}

	public static String[] getVersionList() {
		String[] versionStrings = new String[Version.values().length];
		for (int i = 0; i < Version.values().length; i++) {
			versionStrings[i] = Version.values()[i].version;
		}
		return versionStrings;
	}

	public static Version getVersionForString(String versionString) {
		for (int i = 0; i < Version.values().length; i++) {
			if (Version.values()[i].version.equals(versionString)) {
				return Version.values()[i];
			}
		}
		return null;
	}
}
