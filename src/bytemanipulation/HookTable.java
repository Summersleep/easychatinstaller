package bytemanipulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HookTable {

	public static List<Version> versions = new ArrayList<Version>();

	// Class - EasyChat
	public String EasyChat = "EasyChat";

	// Class - EasyChat, Method - sendChatMessage
	public String EasyChat_sendChatMessage = "sendChatMessage";

	// Class - EasyChat, Method - addNewMessage
	public String EasyChat_addNewMessage = "addNewMessage";

	// Class - Minecraft
	public HashMap<Version, String> Minecraft = new HashMap<Version, String>();

	// Class - Minecraft, Method - getMinecraft
	public HashMap<Version, String> Minecraft_getMinecraft = new HashMap<Version, String>();

	// Class - EntityPlayerSP
	public HashMap<Version, String> EntityPlayerSP = new HashMap<Version, String>();

	// Class - EntityPlayerSP, Method - sendChatMessage
	public HashMap<Version, String> EntityPlayerSP_sendChatMessage = new HashMap<Version, String>();

	// Class - NetHandlerPlayClient
	public HashMap<Version, String> NetHandlerPlayClient = new HashMap<Version, String>();

	// Class - NetHandlerPlayClient, Method - addToSendQueue
	public HashMap<Version, String> NetHandlerPlayClient_addToSendQueue = new HashMap<Version, String>();

	// Class - IChatComponent
	public HashMap<Version, String> IChatComponent = new HashMap<Version, String>();

	// Class - IChatComponent, Method - getUnformatedText
	public HashMap<Version, String> IChatComponent_getUnformatedText = new HashMap<Version, String>();

	// Class - EntityPlayerMP
	public HashMap<Version, String> EntityPlayerMP = new HashMap<Version, String>();

	// Class - EntityPlayerMP, Method - _addChatComponentMessage
	public HashMap<Version, String> EntityPlayerMP_addChatComponentMessage = new HashMap<Version, String>();

	// Class - GameConfiguration
	public HashMap<Version, String> GameConfiguration = new HashMap<Version, String>();

	// Class - GuiNewChat
	public HashMap<Version, String> GuiNewChat = new HashMap<Version, String>();

	// Class - GuiNewChat, Method - printChatMessage
	public HashMap<Version, String> GuiNewChat_printChatMessage = new HashMap<Version, String>();

	public HookTable() {
		Minecraft.put(Version.V1_8_8, "ave");
		Minecraft.put(Version.V1_8_8_LABY_MOD2, "ave");
		Minecraft.put(Version.V1_8_9, "ave");
		Minecraft.put(Version.V1_8_9_LABY_MOD3, "ave");

		GuiNewChat.put(Version.V1_8_8, "avt");
		GuiNewChat.put(Version.V1_8_8_LABY_MOD2, "avt");
		GuiNewChat.put(Version.V1_8_9, "avt");
		GuiNewChat.put(Version.V1_8_9_LABY_MOD3, "avt");

		GuiNewChat_printChatMessage.put(Version.V1_8_8, "a");
		GuiNewChat_printChatMessage.put(Version.V1_8_8_LABY_MOD2, "a");
		GuiNewChat_printChatMessage.put(Version.V1_8_9, "a");
		GuiNewChat_printChatMessage.put(Version.V1_8_9_LABY_MOD3, "a");

		Minecraft_getMinecraft.put(Version.V1_8_8, "A");
		Minecraft_getMinecraft.put(Version.V1_8_8_LABY_MOD2, "A");
		Minecraft_getMinecraft.put(Version.V1_8_9, "A");
		Minecraft_getMinecraft.put(Version.V1_8_9_LABY_MOD3, "A");

		EntityPlayerSP.put(Version.V1_8_8, "bew");
		EntityPlayerSP.put(Version.V1_8_8_LABY_MOD2, "bew");
		EntityPlayerSP.put(Version.V1_8_9, "bew");
		EntityPlayerSP.put(Version.V1_8_9_LABY_MOD3, "bew");

		EntityPlayerSP_sendChatMessage.put(Version.V1_8_8, "e");
		EntityPlayerSP_sendChatMessage.put(Version.V1_8_8_LABY_MOD2, "e");
		EntityPlayerSP_sendChatMessage.put(Version.V1_8_9, "e");
		EntityPlayerSP_sendChatMessage.put(Version.V1_8_9_LABY_MOD3, "e");

		NetHandlerPlayClient.put(Version.V1_8_8, "bcy");
		NetHandlerPlayClient.put(Version.V1_8_8_LABY_MOD2, "bcy");
		NetHandlerPlayClient.put(Version.V1_8_9, "bcy");
		NetHandlerPlayClient.put(Version.V1_8_9_LABY_MOD3, "bcy");

		NetHandlerPlayClient_addToSendQueue.put(Version.V1_8_8, "a");
		NetHandlerPlayClient_addToSendQueue.put(Version.V1_8_8_LABY_MOD2, "a");
		NetHandlerPlayClient_addToSendQueue.put(Version.V1_8_9, "a");
		NetHandlerPlayClient_addToSendQueue.put(Version.V1_8_9_LABY_MOD3, "a");

		IChatComponent.put(Version.V1_8_8, "eu");
		IChatComponent.put(Version.V1_8_8_LABY_MOD2, "eu");
		IChatComponent.put(Version.V1_8_9, "eu");
		IChatComponent.put(Version.V1_8_9_LABY_MOD3, "eu");

		IChatComponent_getUnformatedText.put(Version.V1_8_8, "c");
		IChatComponent_getUnformatedText.put(Version.V1_8_8_LABY_MOD2, "c");
		IChatComponent_getUnformatedText.put(Version.V1_8_9, "c");
		IChatComponent_getUnformatedText.put(Version.V1_8_9_LABY_MOD3, "c");

		EntityPlayerMP.put(Version.V1_8_8, "lf");
		EntityPlayerMP.put(Version.V1_8_8_LABY_MOD2, "lf");
		EntityPlayerMP.put(Version.V1_8_9, "lf");
		EntityPlayerMP.put(Version.V1_8_9_LABY_MOD3, "lf");

		EntityPlayerMP_addChatComponentMessage.put(Version.V1_8_8, "b");
		EntityPlayerMP_addChatComponentMessage.put(Version.V1_8_8_LABY_MOD2, "b");
		EntityPlayerMP_addChatComponentMessage.put(Version.V1_8_9, "b");
		EntityPlayerMP_addChatComponentMessage.put(Version.V1_8_9_LABY_MOD3, "b");

		GameConfiguration.put(Version.V1_8_8, "bao");
		GameConfiguration.put(Version.V1_8_8_LABY_MOD2, "bao");
		GameConfiguration.put(Version.V1_8_9, "bao");
		GameConfiguration.put(Version.V1_8_9_LABY_MOD3, "bao");
	}

}
