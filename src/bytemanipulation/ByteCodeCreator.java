package bytemanipulation;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import javassist.CannotCompileException;
import javassist.ClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;
import main.EasyChat;

public class ByteCodeCreator {

	public static void manipulateLabbyMod3() throws NotFoundException, CannotCompileException, IOException {
		String labyModPath = "C:\\Users\\Micha\\AppData\\Roaming\\.minecraft\\libraries\\net\\labymod\\LabyMod\\3_1.8.9\\LabyMod-3_1.8.9.jar";
		String minecraftJarpath = "C:\\Users\\Micha\\AppData\\Roaming\\.minecraft\\versions\\1.8.9\\1.8.9.jar";

		String GuiNewChatCustom = "net.labymod.core_implementation.mc18.gui.GuiNewChatCustom";
		String GuiExtraChat_setChatLine = "setChatLine";

		ClassPool classPool = ClassPool.getDefault();
		classPool.appendClassPath(labyModPath);
		classPool.appendClassPath(minecraftJarpath);

		CtClass classGuiNewChatCustom = classPool.get(GuiNewChatCustom);

		CtMethod methodSetChatLine = classGuiNewChatCustom.getDeclaredMethod(GuiExtraChat_setChatLine);

		String src = "";
		src += " boolean showInExtraChat = false;";
		src += " String[] forbiddenNames = {\"accept\", \"bank\", \"griefergamesbank\", \"annehmen\",};";
		src += " String extractPlayerIfIsMessagePattern  = \"(?:^\\\\[mir -> (?:[^\\\\ ]*) \\\\| ([^\\\\]]*)\\\\])|(?:^\\\\[(?:[^ ]*) \\\\| ([^ ]*) -> mir\\\\])\";";
		src += " java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(extractPlayerIfIsMessagePattern );";
		src += " java.util.regex.Matcher matcher = pattern.matcher($1.c());";
		src += " while (matcher.find()) {";
		src += " for (int i = 0; i <= matcher.groupCount(); i++) {";
		src += " if (i > 0 && matcher.group(i) != null) {";
		src += " showInExtraChat = true;";
		src += " }}}";
		src += " for (int i = 0; i < forbiddenNames.length; i++) {";
		src += " String[] splits = $1.c().split(\":\");";
		src += " if (splits.length > 0) {";
		src += " if (splits[0].contains(forbiddenNames[i].toLowerCase())) {";
		src += " return;";
		src += " }}}";
		src += " if (showInExtraChat) {";
		src += " net.labymod.core.LabyModCore.getGuiExtraChat().setChatLine((Object)chatComponent, $2, $3, $4);";
		src += " return;";
		src += " }";

		String.format("%-16s -> mir: %s", "", "");
		String.format("mir -> %-16s :", "", "");

		methodSetChatLine.insertBefore(src);

		classGuiNewChatCustom.writeFile("output\\");

	}

	// $0 = this, $1, $2... = parameters from Method or Constructor head
	public static void manipulateByteCodeForMcVersion(Version version, String[] classPathes)
			throws NotFoundException, CannotCompileException, IOException {

		HookTable hookTable = new HookTable();

		ClassPool classPool = ClassPool.getDefault();

		ClassPath[] cps = new ClassPath[classPathes.length];
		for (int i = 0; i < classPathes.length; i++) {
			cps[i] = classPool.appendClassPath(classPathes[i]);

		}

		CtClass string = classPool.get(String.class.getName());

		CtClass minecraft = classPool.get(hookTable.Minecraft.get(version));

		CtClass iChatComponent = classPool.get(hookTable.IChatComponent.get(version));

		CtClass guiNewChat = classPool.get(hookTable.GuiNewChat.get(version));

		CtClass easyChat = classPool.get(hookTable.EasyChat);

		CtClass entityPlayerSP = classPool.get(hookTable.EntityPlayerSP.get(version));

		CtMethod entityPlayerSP_sendChatMessage = entityPlayerSP
				.getDeclaredMethod(hookTable.EntityPlayerSP_sendChatMessage.get(version), new CtClass[] { string });

		CtMethod easyChat_sendChatMessage = easyChat.getDeclaredMethod(hookTable.EasyChat_sendChatMessage,
				new CtClass[] { string });

		CtMethod easyChat_addNewMessage = easyChat.getDeclaredMethod(hookTable.EasyChat_addNewMessage,
				new CtClass[] { string });

		CtMethod iChatComponent_getUnformatedText = minecraft
				.getDeclaredMethod(hookTable.IChatComponent_getUnformatedText.get(version), new CtClass[] {});

		CtMethod guiNewChat_printChatMessage = guiNewChat.getDeclaredMethod(
				hookTable.GuiNewChat_printChatMessage.get(version), new CtClass[] { iChatComponent });

		CtConstructor guiNewChatConstructor = guiNewChat.getDeclaredConstructor(new CtClass[] { minecraft });

		CtConstructor easyChatConstructor = easyChat.getDeclaredConstructor(new CtClass[] {});

		// Easychat Class ------------------------------------------------------

		// Add new Field
		easyChat.addField(new CtField(minecraft, "mc", easyChat));

		// Edit Constructor
		easyChatConstructor.addParameter(minecraft);
		easyChatConstructor.insertAfter("$0.mc = $1;");

		// Edit Method
		CtField field_entityPlayerSP = getFieldByType(minecraft, entityPlayerSP);
		if (field_entityPlayerSP != null) {
			easyChat_sendChatMessage.insertAfter("$0.mc." + field_entityPlayerSP.getFieldInfo().getName() + "."
					+ entityPlayerSP_sendChatMessage.getName() + "($1);");
		}

		// Save edited Class
		easyChat.writeFile(EasyChat.OUTPUT_DIR_PATH);
		easyChat.defrost();

		// GuiNewChat Class ------------------------------------------------------

		// Add new Field
		guiNewChat.addField(new CtField(easyChat, "easyChat", guiNewChat));

		// Edit Constructor
		guiNewChatConstructor.insertAfter("$0.easyChat = new " + easyChat.getName() + "($1);");

		// Edit method
		guiNewChat_printChatMessage.insertAfter("$0.easyChat." + easyChat_addNewMessage.getName() + "($1."
				+ iChatComponent_getUnformatedText.getName() + "());");

		// Save edited Class
		guiNewChat.writeFile(EasyChat.OUTPUT_DIR_PATH);
		guiNewChat.defrost();

		// Remove classes from Classpool tho they are "freed" and can be used by other
		// processes later
		for (int i = 0; i < cps.length; i++) {
			classPool.removeClassPath(cps[i]);
		}

	}

	private static CtField getFieldByType(CtClass mainClazz, CtClass fieldClazz) {
		for (CtField field : mainClazz.getDeclaredFields()) {
			// TODO - Only works if we have only 1 Field with that specific Class in our
			// MainClass. This will compare the Field Type (Classes) not its actual variable
			// name
			try {
				if (field.getType().getName().equals(fieldClazz.getName())) {
					return field;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return null;
	}

	public static <T> boolean listEqualsIgnoreOrder(List<T> list1, List<T> list2) {
		return new HashSet<>(list1).equals(new HashSet<>(list2));
	}

}
